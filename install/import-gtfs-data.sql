\copy raw.agency from 'input/sncf_gtfs/agency.txt' (format csv, header)
\copy raw.calendar from 'input/sncf_gtfs/calendar.txt' (format csv, header)
\copy raw.calendar_dates from 'input/sncf_gtfs/calendar_dates.txt' (format csv, header)
\copy raw.routes from 'input/sncf_gtfs/routes.txt' (format csv, header)
\copy raw.stop_extensions from 'input/sncf_gtfs/stop_extensions.txt' (format csv, header)
\copy raw.stops from 'input/sncf_gtfs/stops.txt' (format csv, header)
\copy raw.stop_times from 'input/sncf_gtfs/stop_times.txt' (format csv, header)
\copy raw.transfers from 'input/sncf_gtfs/transfers.txt' (format csv, header)
\copy raw.trips from 'input/sncf_gtfs/trips.txt' (format csv, header)
