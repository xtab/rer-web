/*
 * NOTE : les clefs et index ne sont pas définis ici mais dans
 * index-rer-web-tables.sql.  Ceci afin de permettre de désactiver
 * temporairement les index lors du chargement de données en masse.
 */

DROP TEXT SEARCH CONFIGURATION IF EXISTS fr_unaccent;

CREATE TEXT SEARCH CONFIGURATION fr_unaccent (COPY = simple);

ALTER TEXT SEARCH CONFIGURATION fr_unaccent
  ALTER MAPPING FOR hword, hword_part, word
WITH unaccent, simple;

CREATE TABLE IF NOT EXISTS metadata (
  key TEXT NOT NULL,
  value TEXT NOT NULL
);

CREATE TABLE station_codes (
  pa_id INTEGER NOT NULL,
  code  TEXT NOT NULL,
  uic   TEXT NOT NULL
);

CREATE TABLE station_names (
  pa_id INTEGER NOT NULL,
  name  TEXT NOT NULL
);

CREATE TABLE station_lines (
  pa_id INTEGER NOT NULL,
  line TEXT NOT NULL,
  line_index INTEGER,
  increasing_index_is_outbound BOOL
);

/*
 * La table de transcodage pour faire les conversions entre UIC7, UIC8 et ID
 * d’arrêt du GTFS.
 */
CREATE TABLE IF NOT EXISTS transco_icar (
  transporteur_nom TEXT NOT NULL,
  pa_id INTEGER NOT NULL,
  uic8 TEXT,
  uic7 TEXT,
  pr_id TEXT,
  zde_id INTEGER NOT NULL,
  zde_libelle TEXT NOT NULL,
  zde_type TEXT NOT NULL,
  zde_mode TEXT NOT NULL,
  zdep_id TEXT NOT NULL,
  zdep_nom TEXT NOT NULL,
  zdep_mode TEXT NOT NULL,
  zder_id TEXT NOT NULL,
  zder_nom TEXT NOT NULL,
  zder_mode TEXT NOT NULL,
  zdlr_id TEXT NOT NULL,
  zdlr_nom TEXT NOT NULL,
  lda_id TEXT NOT NULL,
  zde_associee TEXT);

/*
 * Contient la liste blanche des codes UIC8 utilisables pour interroger l’API
 * Temps Réel Transilien.
 */

CREATE TABLE IF NOT EXISTS valid_transilien_api_uics (
  uic8 TEXT);
