#!/usr/bin/env perl

package RER::Web;

use strict;
use warnings;
use utf8;

use POSIX qw(strftime);

use Dancer ':syntax';
use Dancer::Plugin::Database;

use RER::Cache;
use RER::Transilien;
use RER::Results;
use RER::Gares;
use RER::DataSource::PRIM;
use RER::DataSource::TransilienGTFS;
use Storable qw(dclone);

our $VERSION = '0.1';

my %stats;

sub check_code {
    my ($code) = @_;

    if (defined $code && uc($code) =~ /^(?:[A-Z]{1,3}|NC[1-9]|ND0)$/) {
        return uc($code);
    }
    else {
        return undef;
    }
}



sub stats_add {
    my ($event, $extra_data) = @_;

    my $extra_data_json =
        JSON->new->allow_nonref(1)->convert_blessed(1)->encode($extra_data);

    my $sth = database->prepare('CALL stats_add(?, ?)');
    $sth->execute($event, $extra_data_json);
}

# Expires a given train objet cache entry
sub cache_invalidate {
    my ($key) = @_;

    RER::Cache::cache_del($key);
}

# Sets a given train objet cache entry to a new value
sub cache_set_hash {
    my ($key, $value) = @_;

    RER::Cache::cache_put($key, $value, 60, 60);
}

# Gets a train objet cache entry (or undef if cache miss)
sub cache_get_hash {
    my ($key) = @_;

    my $obj = RER::Cache::cache_get($key);
    if (defined $obj) {
        $obj = RER::Results->new(%$obj);
    }
    return $obj;
}




get '/' => sub {
    # Rediriger (302) vers l'url /?s=<blah> si l’utilisateur a sauvegardé sa
    # dernière gare (et sinon on redirige vers la gare par défaut).
    #
    # Si par hasard le cookie référence une gare non valable, on évite de le
    # rediriger de force vers une page 404 car il serait impossible d’en
    # échapper sans supprimer le cookie !
    if (! defined params->{'s'}) {
        if (my $station = RER::Gares::find(code => check_code(cookie('station')))) {
            return redirect uri_for('/', {s => $station->code});
        }
        else {
            return redirect uri_for('/', {s => 'EVC'})
        }
    }

    # trouver la gare dans la base de données
    my $station = RER::Gares::find(code => check_code(params->{'s'}));
    if (!defined $station) {
        status 'not_found';
        send_file '404.html';
        # l’exécution de la route s’arrête ici
    }

    # positionner le cookie (valable 4 semaines)
    # on y touche dans le code js, donc http_only = 0
    cookie "station" => check_code($station->code),
        expires => '4w',
        http_only => 0;

    template 'rer', {
        origin_station => $station->name,
        origin_code    => $station->code,
        dmaj     => RER::Gares::get_last_update(),
        stations => RER::Gares::get_stations(),
        year     => strftime("%Y", localtime()),
    };
};

get '/json' => sub {
    header 'Cache-Control' => 'no-cache';

    set serializer => 'JSON';

    stats_add 'api_incoming';

    my $station = RER::Gares::find(code => check_code(params->{'s'}));
    if (!defined $station) {
        status 404;
        return { error => 'Gare non trouvée' };
    }

    my $code = $station->code;
    my $line = params->{'l'};

    my $ds = RER::DataSource::PRIM->new(api_token => config->{'prim_token'});
    my $ds2 = RER::DataSource::TransilienGTFS->new(dbh => database);

    my $ret = cache_get_hash($station->code);

    if (! defined $ret) {

        stats_add 'api_sent';

        my $data;
        eval {
            $data = RER::Transilien::new(
                from => $station->code,
                ds   => [ $ds, $ds2 ],
            );
        };
        if (my $err = $@) {
            status 503;
            stats_add 'api_errors';
            cache_invalidate $station->code;

            # log error
            error "$code: $err";

            # return error to client
            return { error => $err };
        } else {
            cache_set_hash($code, $data);
        }
        $ret = dclone $data;
    }

    # Filtrer par ligne si cela est désiré

    if ($line) {
        @{$ret->{trains}} = grep { $_->{ligne} && $_->{ligne} eq $line } @{$ret->{trains}};
    }
    # Limiter à 6 le nombre de trains renvoyés
    if (scalar @{$ret->{trains}} > 6) {
        @{$ret->{trains}} = @{$ret->{trains}}[0..5];
    }

    return $ret;
};

get '/autocomp' => sub {
    header 'Cache-Control' => 'no-cache';

    set serializer => 'JSON';

    my $str = params->{'s'} || '';

    return RER::Gares::get_autocomp($str);
};

true;

# vi:ts=4:sw=4:et:
